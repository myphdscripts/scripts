SAVEIFS=$IFS
# usage repos_directory myemails.txt
rm repos.txt
wd=`pwd`
for i in ${1}/*
do
   cd $i
   repo=`echo $i|sed -e "s:.*/\(.*\):\1:g"`
   echo $repo `git log |head -3|grep Author|sed -e"s:.*<\(.*\)>.*:\1:g"` >>$wd/repos.txt
   cd $wd 
done
rm myrepos.txt
for i in `cat ${2}`
do
echo $i
cat repos.txt|grep $i>>myrepos.txt
done
rm irepos.txt
for i in `cat ${3}`
do
echo $i
cat repos.txt|grep $i>>irepos.txt
done
sort myrepos.txt>smyrepos.txt
sort repos.txt>srepos.txt
sort irepos.txt>sirepos.txt
comm -13 smyrepos.txt srepos.txt>tmp.txt
sort tmp.txt > stmp.txt
comm -13 sirepos.txt tmp.txt>sorepos.txt
rm tmp.txt stmp.txt 
rm mycommits.dat
rm ocommits.dat
for i in `cat smyrepos.txt|cut -f1 -d" "`
do
echo $i
cd repos/$i
log=`git log|grep Author|head -1`
count=`git log|grep Author|wc -l`
tue=`git log  --before="2019-10-31T23:59:00+05:00" --date=local --pretty="format:%ae,%ad"|grep ${4}|cut -f2 -d","|cut -f1,2,3 -d " "|uniq|wc -l`
nottue=`git log  --before="2019-10-31T23:59:00+05:00" --date=local --pretty="format:%ae,%ad"|grep -v ${4}|cut -f2 -d","|cut -f1,2,3 -d " "|uniq|wc -l`
echo $log $count $tue $nottue>>$wd/mycommits.dat
cd $wd
done
for i in `cat sorepos.txt|cut -f1 -d" "`
do
repo=`echo $i|cut -f1 -d" "`
echo $i
cd repos/$i
log=`git log|grep Author|head -1`
count=`git log|grep Author|wc -l`
tue=`git log  --before="2019-10-31T23:59:00+05:00" --date=local --pretty="format:%ae,%ad"|grep ${4}|cut -f2 -d","|cut -f1,2,3 -d " "|uniq|wc -l`
nottue=`git log  --before="2019-10-31T23:59:00+05:00" --date=local --pretty="format:%ae,%ad"|grep -v ${4}|cut -f2 -d","|cut -f1,2,3 -d " "|uniq|wc -l`

echo $log $count $tue $nottue>>$wd/ocommits.dat
cd $wd
done
IFS=$SAVEIFS