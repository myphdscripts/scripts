import subprocess
def transitive_closure(a):
   d={}
   for k,v in a:
       if k in d.keys():
          d[k].add(v)
       else:
          flag = 1 
          for i in d.values():
               if k in i:
                   flag = 0
                   break
          if ( flag == 1):
            d[k]=set()
            d[k].add(v)
   return d
f=open("j2.txt")
l=[]
for i in f:
    l.append(tuple(i.split()))
z=transitive_closure(l)
print(z)