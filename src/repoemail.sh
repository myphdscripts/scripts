SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
echo ${1}
cd ${1}
echo *
for i in * 
do
cd ${1}/$i
email=`git log --date=local|head -4|grep Author|sed -e"s:.*<\(.*\)>.*:\1:g"`
echo $i $email
done
cd ~/datanalysis/test/pg/contentanalysis/distance
IFS=$SAVEIFS