#!/bin/bash 
find . -exec grep -l -i "dist" {} \; 2>/dev/null|grep -v git >files1.txt
find . -exec grep -l -i "time" {} \; 2>/dev/null|grep -v git >files2.txt
find . -exec grep -l -i "large" {} \; 2>/dev/null|grep -v git|grep -v pos >files3.txt
find . -exec grep -l -i "digit" {} \; 2>/dev/null|grep -v git >files4.txt
find . -exec grep -l -i "pos" {} \; 2>/dev/null|grep -v git >files5.txt
find . -exec grep -l -i "mark" {} \; 2>/dev/null|grep -v git >files6.txt
find . -exec grep -l -i "string" {} \; 2>/dev/null|grep -v git >files7.txt
find . -exec grep -l -i "emp" {} \; 2>/dev/null|grep -v git >files8.txt
find . -exec grep -l -i "swap" {} \; 2>/dev/null|grep -v git >files9.txt
find . -exec grep -l -in "file" {} \; 2>/dev/null|grep -v git >files10.txt
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
#Double the length of files
for i in `seq 1 10`
do
for j in `cat files$i.txt`
do
   cat $j $j>tmp
   mv tmp $j
done
for k in `cat files$i.txt`
do
for l in `cat files$i.txt`
do
   diff -l -s -w -B $k $l |grep -i identical |grep -v "<"|grep -v ">" |grep -v ".*/\(.*\)/.*\1" >>diff$i.txt
done 
done
java -jar ~/scripts/j.jar -t 1 -l c/c++ -c `cat files$i.txt`|grep Comparing|grep 100 >out$i.txt
python ~/scripts/cluster.py out$i.txt >> myreport.txt
done
IFS=$SAVEIFS