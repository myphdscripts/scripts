#get repository details  of all the repositories where CCPLAB is owner

import json
import requests
import re
import os
import csv
import git
with open('commitDetails.csv', 'w', newline= '') as f:       #csv file
    f_names=['Commit Name', 'Commit Mesage', 'Commit Author', 'Commit Date&Time']
    thewriter = csv.DictWriter(f, fieldnames=f_names)
    thewriter.writeheader()

    response = requests.get('https://api.bitbucket.org/2.0/repositories?role=member&pagelen=670', auth=('ccplab-bmsce', '4PuXrqxcyuTGnS5awBU7'))
    res = response.json()
    #print(res)
    
    for k in res['values']:     #for each repository
        #print (k['links'])
        try:
            x=k['links']['clone'][0]['href']
            y=x.split("@")
            x="https://ccplab-bmsce:j1br1tv0dy@"+y[1] 
            git.Git(".").clone(x)
        except Exception as e:
            continue