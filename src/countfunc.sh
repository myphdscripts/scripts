rm count.txt
rm error.txt
for i in *.c
do
gcc $i -lm 2>>error.txt
if [ $? -eq 0 ]
then
	count=`nm a.out|grep T|grep -v _|wc -l`
	echo $i $count>>count.txt
	rm a.out
else
	echo $i "compilation error">>count.txt
fi
done