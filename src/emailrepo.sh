rm repos.txt
touch repos.txt
for i in ${1}/*
do
   cd $i
   repo=`echo $i|sed -e "s:.*/\(.*\):\1:g"`
   echo $repo `git log |head -3|grep Author|sed -e"s:.*<\(.*\)>.*:\1:g"`
   cd - 2>&1 >/dev/null
done
